#![feature(core_intrinsics)]
use byteorder::{ByteOrder, LittleEndian, WriteBytesExt};
use core::convert::TryInto;
use std::intrinsics::{rotate_left, rotate_right};

// how many 8-byte words the used word size contains
pub static WORD_SIZE: usize = 32 / 8;

pub static P: u32 = 0xB7E15163;
pub static Q: u32 = 0x9E3779B9;
pub static C: usize = 1;

#[derive(Debug, PartialEq)]
pub enum Rc5Error {
    InvalidKey,
    InvalidRound,
}

/// Derive new RC5 round keys from the secret key
///
/// This function assumes that the block size 64 bits (word size is 32 bits)
pub fn derive_keys(key: &Vec<u8>, rounds: usize) -> Result<Vec<u32>, Rc5Error> {

    // round number must be between 0 - 255
    if rounds > 255 {
        return Err(Rc5Error::InvalidRound);
    }

    // key must be between 0 - 2040 bits
    if key.len() > 255 {
        return Err(Rc5Error::InvalidKey);
    }

    let rkey_len = 2 * (rounds + 1);
    let mut round_keys = vec![0u32; rkey_len];

    // expand the u8 vector to a word vector
    //
    // if the key is empty, make sure to set thet expanded size correctly
    // and not to copy anything from `key`
    let expanded_len = std::cmp::max(key.len() / WORD_SIZE, C);
    let mut expanded = vec![0u32; expanded_len];

    if key.len() > 0 {
        LittleEndian::read_u32_into(&key, &mut expanded);
    }

    // initialize the round keys with a pseudo-random bit pattern
    round_keys[0] = P;
    for i in 1..rkey_len {
        round_keys[i] = round_keys[i - 1].wrapping_add(Q);
    }

    // mix the secret key into the round keys
    let (mut a, mut b, mut i, mut j) = (0, 0, 0, 0);

    // make sure to use correct iteration count in case the input key is large
    let iter_count = if expanded_len > rkey_len {
        expanded_len * 3
    } else {
        rkey_len * 3
    };

    for _k in (0..iter_count).rev() {
        a = rotate_left(round_keys[i].wrapping_add(a).wrapping_add(b), 3);
        round_keys[i] = a;

        b = rotate_left(
            expanded[j].wrapping_add(a).wrapping_add(b),
            a.wrapping_add(b),
        );
        expanded[j] = b;

        i = (i + 1) % rkey_len;
        j = (j + 1) % expanded_len;
    }

    Ok(round_keys)
}

/// RC5-encode `plaintext` input by deriving round keys and applying the encoding routine
///
/// ## Return value
/// RC5-encoded ciphertext
pub fn encode(key: &Vec<u8>, plaintext: &Vec<u8>, rounds: usize) -> Result<Vec<u8>, Rc5Error> {
    let round_keys = derive_keys(key, rounds)?;

    let mut a = u32::from_le_bytes(plaintext[0..WORD_SIZE].try_into().unwrap());
    let mut b = u32::from_le_bytes(plaintext[WORD_SIZE..(WORD_SIZE * 2)].try_into().unwrap());

    a = a.wrapping_add(round_keys[0]);
    b = b.wrapping_add(round_keys[1]);

    for i in 1..(rounds + 1) {
        a = rotate_left(a ^ b, b).wrapping_add(round_keys[2 * i]);
        b = rotate_left(b ^ a, a).wrapping_add(round_keys[2 * i + 1]);
    }

    let mut ciphertext = vec![];
    ciphertext.write_u32::<LittleEndian>(a).unwrap();
    ciphertext.write_u32::<LittleEndian>(b).unwrap();
    Ok(ciphertext)
}

/// RC5-decode `ciphertext` input by deriving round keys and applying the decoding routine
///
/// ## Return value
/// RC5-decoded plaintext
pub fn decode(key: &Vec<u8>, ciphertext: &Vec<u8>, rounds: usize) -> Result<Vec<u8>, Rc5Error> {
    let round_keys = derive_keys(key, rounds)?;

    let mut a = u32::from_le_bytes(ciphertext[0..WORD_SIZE].try_into().unwrap());
    let mut b = u32::from_le_bytes(ciphertext[WORD_SIZE..(WORD_SIZE * 2)].try_into().unwrap());

    for i in (1..(rounds + 1)).rev() {
        b = rotate_right(b.wrapping_sub(round_keys[2 * i + 1]), a) ^ a;
        a = rotate_right(a.wrapping_sub(round_keys[2 * i]), b) ^ b;
    }

    b = b.wrapping_sub(round_keys[1]);
    a = a.wrapping_sub(round_keys[0]);

    let mut plaintext = vec![];
    plaintext.write_u32::<LittleEndian>(a).unwrap();
    plaintext.write_u32::<LittleEndian>(b).unwrap();
    Ok(plaintext)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn encode_a() {
        let key = vec![
            0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D,
            0x0E, 0x0F,
        ];
        let pt = vec![0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77];
        let ct = vec![0x2D, 0xDC, 0x14, 0x9B, 0xCF, 0x08, 0x8B, 0x9E];

        let res = encode(&key, &pt, 12).unwrap();
        assert!(&ct[..] == &res[..]);
    }

    #[test]
    fn encode_b() {
        let key = vec![
            0x2B, 0xD6, 0x45, 0x9F, 0x82, 0xC5, 0xB3, 0x00, 0x95, 0x2C, 0x49, 0x10, 0x48, 0x81,
            0xFF, 0x48,
        ];
        let pt = vec![0xEA, 0x02, 0x47, 0x14, 0xAD, 0x5C, 0x4D, 0x84];
        let ct = vec![0x11, 0xE4, 0x3B, 0x86, 0xD2, 0x31, 0xEA, 0x64];

        let res = encode(&key, &pt, 12).unwrap();
        assert!(&ct[..] == &res[..]);
    }

    #[test]
    fn decode_a() {
        let key = vec![
            0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D,
            0x0E, 0x0F,
        ];
        let pt = vec![0x96, 0x95, 0x0D, 0xDA, 0x65, 0x4A, 0x3D, 0x62];
        let ct = vec![0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77];

        let res = decode(&key, &ct, 12).unwrap();
        assert!(&pt[..] == &res[..]);
    }

    #[test]
    fn decode_b() {
        let key = vec![
            0x2B, 0xD6, 0x45, 0x9F, 0x82, 0xC5, 0xB3, 0x00, 0x95, 0x2C, 0x49, 0x10, 0x48, 0x81,
            0xFF, 0x48,
        ];
        let pt = vec![0x63, 0x8B, 0x3A, 0x5E, 0xF7, 0x2B, 0x66, 0x3F];
        let ct = vec![0xEA, 0x02, 0x47, 0x14, 0xAD, 0x5C, 0x4D, 0x84];

        let res = decode(&key, &ct, 12).unwrap();
        assert!(&pt[..] == &res[..]);
    }

    #[test]
    fn test_1024_bit_keys() {
        let key = vec![
            0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x0C, 0x0D, 0x0E, 0x0F, 0x00, 0x11,
            0x22, 0x33, 0x0C, 0x0D, 0x0E, 0x0F, 0x00, 0x11, 0x22, 0x33, 0x0C, 0x0D, 0x0E, 0x0F,
            0x00, 0x11, 0x22, 0x33, 0x0C, 0x0D, 0x0E, 0x0F, 0x00, 0x11, 0x22, 0x33, 0x0C, 0x0D,
            0x0E, 0x0F, 0x00, 0x11, 0x22, 0x33, 0x0C, 0x0D, 0x0E, 0x0F, 0x00, 0x11, 0x22, 0x33,
            0x0C, 0x0D, 0x0E, 0x0F, 0x00, 0x11, 0x22, 0x33, 0x0C, 0x0D, 0x0E, 0x0F, 0x00, 0x11,
            0x22, 0x33, 0x0C, 0x0D, 0x0E, 0x0F, 0x00, 0x11, 0x22, 0x33, 0x0C, 0x0D, 0x0E, 0x0F,
            0x00, 0x11, 0x22, 0x33, 0x0C, 0x0D, 0x0E, 0x0F, 0x00, 0x11, 0x22, 0x33, 0x0C, 0x0D,
            0x0E, 0x0F, 0x00, 0x11, 0x22, 0x33, 0x0C, 0x0D, 0x0E, 0x0F, 0x00, 0x11, 0x22, 0x33,
            0x0C, 0x0D, 0x0E, 0x0F, 0x00, 0x11, 0x22, 0x33, 0x0C, 0x0D, 0x0E, 0x0F, 0x00, 0x11,
            0x22, 0x33,
        ];
        let pt = vec![0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77];
        let ct = vec![0x35, 0x05, 0xef, 0xcf, 0x9e, 0xae, 0xf7, 0x7f];

        let res = encode(&key, &pt, 12).unwrap();
        assert!(&ct[..] == &res[..]);
    }

    #[test]
    fn test_24_rounds() {
        let key = vec![
            0x2B, 0xD6, 0x45, 0x9F, 0x82, 0xC5, 0xB3, 0x00, 0x95, 0x2C, 0x49, 0x10, 0x48, 0x81,
            0xFF, 0x48,
        ];
        let pt = vec![0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77];
        let ct = vec![0xb1, 0xad, 0x6f, 0x74, 0x89, 0x8f, 0x4b, 0x97];

        let res = encode(&key, &pt, 24).unwrap();
        assert!(&ct[..] == &res[..]);
    }

    // from https://tools.ietf.org/id/draft-krovetz-rc6-rc5-vectors-00.html#rfc.section.4
    #[test]
    fn test_rc5_32_20_16() {
        let key = vec![
            0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D,
            0x0E, 0x0F,
        ];
        let pt = vec![0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07];
        let ct = vec![0x2A, 0x0E, 0xDC, 0x0E, 0x94, 0x31, 0xFF, 0x73];

        let res = encode(&key, &pt, 20).unwrap();
        assert!(&ct[..] == &res[..]);
    }

    #[test]
    fn test_zero_rounds() {
        let key = vec![
            0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E,
            0x0F,
        ];
        let pt = vec![0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77];
        let ct = vec![0x63, 0x55, 0x31, 0x9d, 0x13, 0x2a, 0xff, 0x61];

        let res = encode(&key, &pt, 0).unwrap();
        assert!(&ct[..] == &res[..]);
    }

    #[test]
    fn test_empty_key() {
        let key = vec![];
        let pt  = vec![ 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07 ];
        let ct  = vec![ 0xD7, 0x86, 0xE2, 0x26, 0xDB, 0x66, 0x27, 0x8E ];

        let res = encode(&key, &pt, 12).unwrap();
        assert!(&ct[..] == &res[..]);
    }

    #[test]
    fn test_invalid_data() {
        let key = vec![0u8; 2048 / 8]; // one byte longer key than allowed
        let key2 = vec![0u8; 16];
        let pt = vec![0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07];
        let ct = vec![0x2A, 0x0E, 0xDC, 0x0E, 0x94, 0x31, 0xFF, 0x73];

        assert_eq!(encode(&key, &pt, 20), Err(Rc5Error::InvalidKey));

        // invalid number of rounds
        assert_eq!(encode(&key, &pt, 384), Err(Rc5Error::InvalidRound));
    }
}
